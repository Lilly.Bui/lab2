package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random; 

    public Pokemon(String name){
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if (this.healthPoints <= 0){
            return false;
        } else{
            return true;
        }
    }

    @Override
    public void attack(IPokemon target) {
        if(this.isAlive()){
            int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
            if(damageInflicted > 0){
                System.out.println(this.getName() + " attacks " + target.getName());
                target.damage(damageInflicted);
                if (target.isAlive() == false){
                    System.out.println(target.getName() + " is defeated by " + this.getName());
                }
            }
        }
    }

    @Override
    public void damage(int damageTaken) {
        if(damageTaken > 0){
            healthPoints = healthPoints - damageTaken;
            if (healthPoints < 0){
                healthPoints = 0;
            } 
            String s1 = String.format("%s takes %d damage and is left with %d/%d HP", name, damageTaken, healthPoints, maxHealthPoints);
            System.out.println(s1);
        }
    }

    @Override
    public String toString() {
        return name + " HP: " + "(" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength;
    }

}
